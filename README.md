# Docker Images

Images are represented by first level folders (e.g., `maven`).
Tags are defined by second level folders (e.g., `3.5.4`).

```
...
├── mailcatcher
│   └── 0.7.1
│       └── Dockerfile
├── maven
│   ├── 3.5.4
│   │   ├── Dockerfile
│   │   ├── mavenrc
│   │   ├── settings.xml
│   │   └── ssh-config
│   └── 3.5.4-jdk-11
│       ├── Dockerfile
│       ├── mavenrc
│       ├── settings.xml
│       └── ssh-config
...
```

This structure unfortunately creates duplicated files (e.g. `maven/3.5.4` vs. `maven/3.5.4-jdk-11`),
but we believe that the following advantages outweigh the disadvantage of redundancy.

- The `master` branch gives a good overview of available images
- Changes of the `.gitlab-ci.yml` doesn't have to be populated via cherry-picks
- Image tags are not spread over different branches
- Image tags can be compared easily
- Fixes of different tags can be achieved by single commits

Every image will be build on the `master` branch (to continuously verifying the building process), but pushed only on the related release branch (e.g. `release/maven/3.5.4`).

Our image names are prefixed by the url of the container registry (`registry.gitlab.com`) and the namespace of this project (`scce/docker-images`).
The command to pull a certain image (e.g. `maven:3.5.4`) would look like this:

```shell
docker pull registry.gitlab.com/scce/docker-images/maven:3.5.4
```

## Workflow

To ensure stability the `master` and all release branches (e.g. `maven/3.5.4`) are protected.
The workflow roughly follows the [GitLab Flow](https://docs.gitlab.com/ce/topics/gitlab_flow.html).

1. Prepare the changes to the image in a separated feature branch (preferable by creating an issue and related merge request branch)
2. Build and test your image locally before pushing to the feature branch   
3. Let your changes be reviewed by a peer team member
4. Create a merge request from your feature branch into the `master` branch and ask a maintainer to accept it
5. Make certain that the [pipeline](https://gitlab.com/scce/docker-images/-/pipelines) succeeded
6. Create a merge request from `master` into the release branch of the image you just changed (e.g. `release/maven/3.5.4`)

## Tags 

Please find the available images and tags in the [container registry](https://gitlab.com/scce/docker-images/container_registry).

- Semantic versioning for stable versions, e.g. `2.1.3`
- Suffixed commit hash for exact image builds e.g. `2.1.3-2addce1d8e3516292d4e19e22d9b93d10fa06cbe`
